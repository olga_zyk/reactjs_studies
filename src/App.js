import React, { Component } from 'react';
import {PageHeader, Grid } from 'react-bootstrap';
// import logo from './logo.svg';
import NoteItem from './NoteItem.js';
import './App.css';


class App extends Component {
  constructor(props) {
    super(props);
  }
  
  


  render() {
    return (
      <div className="container">
      <PageHeader>
        <h3>Bulletin board</h3>
      </PageHeader>
      <Grid fluid>
        <NoteItem />
      </Grid>
      </div>
    );


  }

}


export default App;
