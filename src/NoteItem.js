import React, { Component } from 'react';
import { Col, Panel, ListGroup, ListGroupItem, Button, Glyphicon, FormGroup, FormControl } from 'react-bootstrap';

class NoteItem extends Component {
  constructor(props) {
    super(props);

    
    this.state = {editing: false};
    this.state = {inputValue: 'Change me'};
    this.edit = this.edit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.remove = this.remove.bind(this);
    this.save = this.save.bind(this);

  }

  getInitialState() {
    return ( {
      editing: false
      // inputValue: ''
    });
  }  

  handleChange(event) {
    this.setState({
      // ** Update "text" property with new value
      inputValue: event.target.value
    });
  }

  edit() {
    this.setState( {
      editing: true
    });
  }

  remove() {
    // this.setState(

    // );
    console.log('called remove()');
    this.props.onItemDelete(this.props.value);
  }

  save() {
    // @TODO: actually save some data, at least in memory
    this.setState( {
      editing: false
    });
  }

  renderDisplay() {
    return (
        <Col xs={3} md={3} lg={3}>
          <Panel footer={this.props.button} bsStyle="success">
            <ListGroup fill>
              <ListGroupItem>
                <p>{this.props.children}</p>
              </ListGroupItem>
              <span className="pull-right">
                <Button bsStyle="primary" bsSize="xs" onClick={this.edit}><Glyphicon glyph="pencil" /></Button>&nbsp;
                <Button bsStyle="danger" bsSize="xs" onClick={this.remove}><Glyphicon glyph="remove" /></Button>
              </span>
            </ListGroup>
          </Panel>
        </Col>
    );
  }

  renderForm() {
    return (
        <Col xs={3} md={3} lg={3}>
          <FormGroup controlId="formControlsTextarea">
            <FormControl componentClass="textarea" type="text" value={this.state.inputValue} onChange={this.handleChange} />
            <Button className="pull-right" bsStyle="success" bsSize="xs" onClick={this.save}><Glyphicon glyph="floppy-disk" /></Button>
          </FormGroup>
        </Col>
    );
  }

  render () {
    if (this.state.editing) {
      return (
        this.renderForm()
      )
    } else {
      return (
        this.renderDisplay()
      )
    }
  }
      
}
  

export default NoteItem; 